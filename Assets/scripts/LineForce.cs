using UnityEngine;

public class LineForce : MonoBehaviour
{
    public Vector3 originalPos;
    public Vector3 posHoyo2;
    public Vector3 posHoyo3;
    public Vector3 posHoyo4;
    public Vector3 posHoyo5;

    bool hoyo1 = false;
    bool hoyo2 = false;
    bool hoyo3 = false;
    bool hoyo4 = false;
    bool hoyo5 = false;


    [SerializeField] private float shotPower;
    [SerializeField] private float stopVelocity = .05f; //The velocity below which the rigidbody will be considered as stopped

    [SerializeField] private LineRenderer lineRenderer;

    private bool isIdle;
    private bool isAiming;

    private Rigidbody rigidbody;


    void Start()
    {
        
        originalPos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        /*
        if (hoyo1 = true)
        {
            originalPos = posHoyo2;
            hoyo1 = false;
        }
        if (hoyo2 = true)
        {
            originalPos = posHoyo3;
            hoyo2 = false;
        }
        if (hoyo3 = true)
        {
            originalPos = posHoyo4;
            hoyo3 = false;
        }
        if (hoyo4 = true)
        {
            originalPos = posHoyo5;
            hoyo4 = false;
        }
        if (hoyo5 = true)
        {
            originalPos = originalPos;
            hoyo5 = false;
        }
        */

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            /*
            if ((hoyo1 = true)&&(hoyo2 = false)) 
            {
                gameObject.transform.position = posHoyo2;
                rigidbody.velocity = Vector3.zero;

            }
            if ((hoyo2 = true)&&(hoyo3=false))
            {
                gameObject.transform.position = posHoyo3;
                rigidbody.velocity = Vector3.zero;

            }
            if ((hoyo3 = true)&&(hoyo4=false))
            {
                gameObject.transform.position = posHoyo4;
                rigidbody.velocity = Vector3.zero;

            }
            if ((hoyo4 = true)&&(hoyo5=false))
            {
                gameObject.transform.position = posHoyo5;
                rigidbody.velocity = Vector3.zero;

            }
            if (hoyo5 = true)
            {
                gameObject.transform.position = posHoyo5;
                rigidbody.velocity = Vector3.zero;

            }
            */
            gameObject.transform.position = originalPos;
            rigidbody.velocity = Vector3.zero;
        }

    }


    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();

        isAiming = false;
        lineRenderer.enabled = false;
    }

    private void FixedUpdate()
    {
        if (rigidbody.velocity.magnitude < stopVelocity)
        {
            Stop();
        }

        ProcessAim();
    }

    private void OnMouseDown()
    {
        if (isIdle)
        {
            isAiming = true;
        }
    }

    private void ProcessAim()
    {
        if (!isAiming || !isIdle)
        {
            return;
        }

        Vector3? worldPoint = CastMouseClickRay();

        if (!worldPoint.HasValue)
        {
            return;
        }

        DrawLine(worldPoint.Value);

        if (Input.GetMouseButtonUp(0))
        {
            Shoot(worldPoint.Value);
        }
    }

    private void Shoot(Vector3 worldPoint)
    {
        isAiming = false;
        lineRenderer.enabled = false;

        Vector3 horizontalWorldPoint = new Vector3(worldPoint.x, transform.position.y, worldPoint.z);

        Vector3 direction = (horizontalWorldPoint - transform.position).normalized;
        float strength = Vector3.Distance(transform.position, horizontalWorldPoint);

        rigidbody.AddForce(direction * strength * shotPower);
        isIdle = false;
    }

    private void DrawLine(Vector3 worldPoint)
    {
        Vector3[] positions = {
            transform.position,
            worldPoint};
        lineRenderer.SetPositions(positions);
        lineRenderer.enabled = true;
    }

    private void Stop()
    {
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
        isIdle = true;
    }

    private Vector3? CastMouseClickRay()
    {
        Vector3 screenMousePosFar = new Vector3(
            Input.mousePosition.x,
            Input.mousePosition.y,
            Camera.main.farClipPlane);
        Vector3 screenMousePosNear = new Vector3(
            Input.mousePosition.x,
            Input.mousePosition.y,
            Camera.main.nearClipPlane);
        Vector3 worldMousePosFar = Camera.main.ScreenToWorldPoint(screenMousePosFar);
        Vector3 worldMousePosNear = Camera.main.ScreenToWorldPoint(screenMousePosNear);
        RaycastHit hit;
        if (Physics.Raycast(worldMousePosNear, worldMousePosFar - worldMousePosNear, out hit, float.PositiveInfinity))
        {
            return hit.point;
        }
        else
        {
            return null;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "End")
        {
            gameObject.transform.position = originalPos;
            rigidbody.velocity = Vector3.zero;
            //hoyo1 = true;
        }
        if (other.gameObject.tag == "EndHoyo")
        {
            gameObject.transform.position = posHoyo2;
            rigidbody.velocity = Vector3.zero;
            originalPos = posHoyo2;
            //hoyo1 = false ;
            //hoyo2 = true;
        }
        else if (other.gameObject.tag == "EndHoyo2")
        {
            gameObject.transform.position = posHoyo3;
            rigidbody.velocity = Vector3.zero;
            originalPos = posHoyo3;
            //hoyo2 = false;
            //hoyo3 = true;
        }
        else if (other.gameObject.tag == "EndHoyo3")
        {
            gameObject.transform.position = posHoyo4;
            rigidbody.velocity = Vector3.zero;
            originalPos = posHoyo4;
            //hoyo3 = false;
            //hoyo4 = true;
        }
        else if (other.gameObject.tag == "EndHoyo4")
        {
            gameObject.transform.position = posHoyo5;
            rigidbody.velocity = Vector3.zero;
            originalPos = posHoyo5;
            //hoyo4 = false;
            //hoyo5 = true;
        }
        else if (other.gameObject.tag == "EndHoyo5")
        {
            gameObject.transform.position = originalPos;
            rigidbody.velocity = Vector3.zero;
            //hoyo5 = false;
            //hoyo1 = true;
        }
    }


}
